from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import LabelEncoder, StandardScaler,OneHotEncoder,MinMaxScaler
from imblearn.pipeline import Pipeline, make_pipeline
from imblearn.over_sampling import SMOTE



pipe = Pipeline(
    [
       
        # == CATEGORICAL ENCODING
        (
            "OneHotEncoder",
            OneHotEncoder(
                handle_unknown="ignore"
            ),
        ),
           
       # == Balancing ==
        (
            "SMOTE Upsampling",
            SMOTE(),

        ),

        ## == Scaling ==

            ("scaler", 
            StandardScaler(
                with_mean=False
            ),
        ),
        # == Decision Tree Classifier ==
        (
            "DecisionTree",
            DecisionTreeClassifier(
                criterion='gini',
                 class_weight='balanced'
            ),
        ),
    ]
)
