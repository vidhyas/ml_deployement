import numpy as np
from config.core import config
from pipeline import pipe
from processing.data_manager import load_dataset, save_pipeline
from sklearn.model_selection import train_test_split


def run_training() -> None:
    """Train the model."""

    # read training data
    data = load_dataset(file_name=config.app_config.training_data_file)
    data.replace(to_replace=['normal.'], value='Normal', inplace=True)
    data.replace(to_replace=['ftp_write.', 'guess_passwd.', 'imap.', 'multihop.', 'phf.', 'spy.', 'warezclient.', 'warezmaster.'],value='R2LAttack', inplace=True)
    data.replace(to_replace=['buffer_overflow.', 'loadmodule.', 'perl.', 'rootkit.'], value='U2RAttack', inplace=True)
    data.replace(to_replace=['ipsweep.', 'portsweep.', 'nmap.', 'satan.'], value='ProbeAttack', inplace=True)
    data.replace(to_replace=['back.', 'land.', 'neptune.', 'pod.', 'smurf.', 'teardrop.'], value='DOSAttack', inplace=True)

    # divide train and test
    X_train, X_test, y_train, y_test = train_test_split(
        data[config.model_config.features],  # predictors
        data[config.model_config.target],
        test_size=config.model_config.test_size,
        # we are setting the random seed here
        # for reproducibility
        random_state=config.model_config.random_state,
    )
    #y_train = np.log(y_train)

    # fit model
    pipe.fit(X_train, y_train)

    # persist trained model
    save_pipeline(pipeline_to_persist=pipe)


if __name__ == "__main__":
    run_training()
