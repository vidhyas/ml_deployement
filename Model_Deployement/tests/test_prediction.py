import math

import numpy as np

from model.predict import make_prediction


def test_make_prediction(sample_input_data):
    # Given
    expected_first_prediction_value_type = str
    expected_no_predictions = 311028
    expected_predictions = ['Normal','R2LAttack','U2RAttack','ProbeAttack','DOSAttack']

    # When
    result = make_prediction(input_data=sample_input_data)

    # Then
    predictions = result.get("predictions")
    assert isinstance(predictions, list)
    assert isinstance(predictions[0], str)
    assert result.get("errors") is None
    assert len(predictions) == expected_no_predictions
    assert predictions[0] in expected_predictions
